<?php
require('vehicle.class.php');

class Bicycle extends Vehicle {
    public $brand;

    public function __construct($brand) {
      $this->brand = $brand;
    }

    public function set_name($brand) {
      $this->brand = $brand;
    }

    public function get_name() {
      return $this->brand;
    }

    public function isMoving($param){
      if($param==0){
        echo 'The bicycle is not moving';
      } else if($param==1){
        echo 'The bicycle is moving';
      } else {
        echo 'Unknown action';
      }
    }
  }
?>