<?php
require('vehicle.class.php');

class Car extends Vehicle {
    public $manufacturer;

    public function __construct($manufacturer) {
      $this->manufacturer = $manufacturer;
    }

    public function set_name($manufacturer) {
      $this->manufacturer = $manufacturer;
    }

    public function get_name() {
      return $this->manufacturer;
    }

    public function isOn($param){
      if($param==0){
        echo 'The car is not started';
      } else if($param==1){
        echo 'The car is started';
      } else {
        echo 'Unknown action';
      }
    }
  }
?>