<?php
require('transport.class.php');

class Vehicle extends Transport {
    public $model;

    public function __construct($model) {
      $this->model = $model;
    }

    public function set_name($model) {
      $this->model = $model;
    }
      
    public function get_name() {
      return $this->model;
    }
  }
?>