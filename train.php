<?php
include('transport.class.php');

class Train extends Transport {
    public $railway;

    public function __construct($railway) {
      $this->railway = $railway;
    }

    public function set_name($railway) {
      $this->railway = $railway;
    }

    public function get_name() {
      return $this->railway;
    }
  }
?>