<?php
include('transport.class.php');

class Plain extends Transport {
    public $airline;

    public function __construct($airline) {
      $this->airline = $airline;
    }

    public function set_name($airline) {
      $this->airline = $airline;
    }

    public function get_name() {
      return $this->airline;
    }
  }
?>